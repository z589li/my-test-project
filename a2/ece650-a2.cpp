// Compile with c++ ece650-a2cpp -std=c++11 -o ece650-a2
#include <vector>
#include <iostream>
#include <list>
#include <sstream>
#include <map>
#include <stdlib.h>

#include <string>
#include <cstddef>

using namespace std;
//a namespace used to extract numbers from string(e.g <1,3>,<3,2>,<3,4>,<4,5>,<5,2>)
namespace strg {
    template<typename Container>
    inline std::size_t strtok(std::string &str, Container &cont, const std::string defstr = " ") {
        cont.clear();
        std::size_t size = 0;
        std::size_t begpos = 0;
        std::size_t endpos = 0;
        begpos = str.find_first_not_of(defstr);
        while (begpos != std::string::npos) {
            size++;
            endpos = str.find_first_of(defstr, begpos);
            if (endpos == std::string::npos) {
                endpos = str.size();
            }
            std::string ssubstr = str.substr(begpos, endpos - begpos);
            cont.push_back(ssubstr);
            begpos = str.find_first_not_of(defstr, endpos + 1);
        }
        return size;
    }

    template<typename Container>
    inline size_t stringtok(std::string &str, Container &cont, const std::string defstr = " ") {
        cont.clear();
        std::size_t size = 0;
        std::size_t length = str.length();

        std::size_t begpos = 0;
        std::size_t endpos = 0;
        while (begpos < length) {
            size++;
            begpos = str.find_first_not_of(defstr, begpos);
            if (begpos == std::string::npos)
                return -1;
            endpos = str.find_first_of(defstr, begpos);
            if (endpos == std::string::npos) {
                endpos = length;
            }
            std::string ssubstr = str.substr(begpos, endpos - begpos);
            cont.push_back(ssubstr);

            begpos = endpos + 1;
        }
        return size;
    }
}

//store all linked vertex for each vertex(e.g adj[1]{3}, adj[2]{3, 5}, adj[3]{2, 4}...)
void addEdge(vector<int> adj[], int src, int dest) {
    adj[src].push_back(dest);
    adj[dest].push_back(src);
}

//using BFS to solve the problem
bool BFS(vector<int> adj[], int src, int dest, int v,
         int pred[], int dist[]) {

    //queue used to traverse the current level of vertex
    list<int> queue;

    //store the vertex if it is visited
    bool *visited = new bool[v];

    for (int i = 0; i < v; i++) {
        visited[i] = false;
        dist[i] = INT8_MAX;
        pred[i] = -1;
    }

    // the source is visited and dist from the source is now 0
    visited[src] = true;
    dist[src] = 0;
    queue.push_back(src);

    while (!queue.empty()) {
        int u = queue.front();
        queue.pop_front();
        for (unsigned int i = 0; i < adj[u].size(); i++) {
            if (visited[adj[u][i]] == false) {
                visited[adj[u][i]] = true;
                dist[adj[u][i]] = dist[u] + 1;
                pred[adj[u][i]] = u;
                queue.push_back(adj[u][i]);

                //stop searching if we find the destination
                if (adj[u][i] == dest)
                    return true;
            }
        }
    }
    //release memory
    delete[] visited;
    return false;
}

//print one of the shortest path is exist
void printShortestDistance(vector<int> adj[], int s,
                           int dest, int v) {

    // predecessor[i] array stores predecessor of i and distance array stores distance of i from s
    int *pred = new int[v];
    int *dist = new int[v];

    if (BFS(adj, s, dest, v, pred, dist) == false) {
        std::cerr << "Error: shortest path vertices exist, but a path does not exist between them." << endl;
        return;
    }

    // vector path stores the shortest path
    vector<int> path;
    int crawl = dest;
    path.push_back(crawl);
    while (pred[crawl] != -1) {
        path.push_back(pred[crawl]);
        crawl = pred[crawl];
    }

    // distance from source is in distance array
    // cout << "length of the shortest path  is : " << dist[dest] << endl;

    // printing path from source to destination
    // cout << "Path is:" << endl;
    for (int i = path.size() - 1; i >= 0; i--) {
        if (i != 0) {
            cout << path[i] << "-";
        } else {
            cout << path[i] << "";
        }
    }
    cout << endl;
    //release memory
    delete[] pred;
    delete[] dist;
}

// Driver program to test above functions
int main() {

    while (!std::cin.eof()) {
        //edges map
        multimap<int, int> edges;
        // separator character
        //const char comma = ',';

        //std::cout << "please input a command: " << endl;
        std::string line;
        std::getline(std::cin, line);
        //if nothing was read, go to top of the while to check for eof
        if (line.size() == 0) {
            continue;
        }
        char cmd = line.at(0);
        line.erase(0, 1);
        //去除开始的空格
        line.erase(0, line.find_first_not_of(" "));
        std::string vertice;
        std::string edge;
        std::string path;
        //flag_1: if the spots in edge is larger then V
        bool flag_1 = true;
        vector<int> edge_vector;
        if (cmd == 'V') {
            vertice = line;
            std::getline(std::cin, edge);
            char cmd_2 = edge.at(0);
            edge.erase(0, 1);
            //dedup the first space
            edge.erase(0, edge.find_first_not_of(" "));
            if (cmd_2 == 'E') {
                //edge = line;
                std::string edge_test = edge;
                edge_test.erase(0, 1);
                edge_test.erase(edge_test.length() - 1, 1);
                vector<string> edge_vector_str;

                int size = strg::stringtok(edge_test, edge_vector_str, "<>, ");
                for (int i = 0; i < size; i++) {
                    int ele = atoi(edge_vector_str[i].c_str());
                    edge_vector.push_back(ele);
                }
                for (unsigned int i = 0; i < edge_vector.size(); ++i) {
                    if (edge_vector[i] > atoi(vertice.c_str())) {
                        std::cerr << "vertex: " << edge_vector[i] << " does not exist" << endl;
                        flag_1 = false;
                        break;
                    }
                }
                if (flag_1 == false) {
                    continue;
                } else {
                    std::getline(std::cin, path);
                    // char cmd_3 = path.at(0);
                    path.erase(0, 1);
                    // dedup the first space
                    path.erase(0, path.find_first_not_of(" "));
                }
            }
        }
        edge.erase(0, 1);
        edge.erase(edge.length() - 1, 1);
        vector<string> vec;
        int size = strg::stringtok(edge, vec, "<>, ");
        for (int i = 0; i < size; i += 2) {
            int key = atoi(vec[i].c_str());
            int value = atoi(vec[i + 1].c_str());
            //edge_vector.push_back(key);
            //edge_vector.push_back(value);
            edges.insert(std::make_pair(key, value));
        }
        //cout << endl;
        //if the vertex in the edges is larger then V
        //bool flag_1 = true;
        /*for (int i = 0; i < edge_vector.size(); ++i) {
            if (edge_vector[i] > atoi(vertice.c_str())) {
                std::cerr << "vertex: " << edge_vector[i] << " does not exist" << endl;
                flag_1 = false;
                break;
            }
        }*/
        //flag_2: if the vertex does not exist
        bool flag_2 = false;
        vector<int> path_vector;
//        path_vector.push_back(path.at(0));
//        path_vector.push_back(path.at(2));
        vector<string> vec2;
        //int size2 = strg::stringtok(path, vec2, " ");
        strg::stringtok(path, vec2, " ");
        int source = atoi(vec2[0].c_str());
        int dest = atoi(vec2[1].c_str());
        for (unsigned int i = 0; i < edge_vector.size(); ++i) {
//            int start = (int)path_vector[0] - '0';
            if (edge_vector[i] == source) {
                for (unsigned int j = 0; j < edge_vector.size(); ++j) {
//                    int end = (int)path_vector[1] - '0';
                    if (edge_vector[j] == dest) {
                        flag_2 = true;
                        break;
                    }
                }
            }
        }
        if (flag_2 == false) {
            std::cerr << "Error: shortest path to a vertex that does not exist" << endl;
        }
        if (flag_1 && flag_2) {
            int v = atoi(vertice.c_str()) + 1;
            vector<int> *adj = new vector<int>[atoi(vertice.c_str()) + 1];
            //multimap<int, int>::iterator beg, end;
            for (auto it = edges.begin(); it != edges.end(); it = edges.upper_bound(it->first)) {
                // key: it->first
                std::pair<std::multimap<int, int>::iterator, std::multimap<int, int>::iterator> lu = edges.equal_range(
                        it->first);
                for (auto j = lu.first; j != lu.second; j++) {
                    //std::cout << "first: " << j->first << " and second: " << j->second << std::endl;
                    addEdge(adj, j->first, j->second);
                }
            }
//            vector<string> vec2;
//            int size2 = strg::stringtok(path, vec2, " ");
//            int source = atoi(vec2[0].c_str());
//            int dest = atoi(vec2[1].c_str());
            printShortestDistance(adj, source, dest, v);
            //return 0;
        }
    }
}
